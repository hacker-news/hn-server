FROM mhart/alpine-node:12.18.0

WORKDIR /hn-server

COPY package*.json . ./

RUN npm install

COPY . /hn-server/

EXPOSE 3001

CMD ["npm", "start"]