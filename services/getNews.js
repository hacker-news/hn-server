const axios = require('axios');
const _ = require('lodash');
const {loadNews} = require('../context/newsRepository')
const config = require('../appsettings.json');

const getNews = async () =>{
  try{
    let hnNews = [];
    const response = await axios.get(config.HNApi.Url);
    let newsFilter = response.data.hits
    .filter(article => article.story_title || article.title);

    newsFilter.forEach(article => {
      hnNews.push( _.omit(article, ['_tags', '_highlightResult']));
    });

    await loadNews(hnNews)
  }catch(e){
    console.log(e);
  }
}

module.exports = getNews;