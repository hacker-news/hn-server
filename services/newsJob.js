const CronJob = require('cron').CronJob;
const getNews = require('./getNews');
//0 ${new Date().getMinutes()} */1 * * * 
let job = new CronJob(`${new Date().getSeconds()} ${new Date().getMinutes()} */1 * * *`,
  () =>  getNews()
 , 
  null, false, undefined, undefined, true);

module.exports = job;