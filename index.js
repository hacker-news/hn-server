require('dotenv').config()
const dbConnection = require('./dbConnection')
const start = require('./app/app')
const {eventEmitter} = require('./events/event-emitter')

dbConnection
.once('open', () => {
  start(eventEmitter)
})