
const express = require("express");
var SSE = require('express-sse');
var sse = new SSE();
const app = express();
const cors = require("cors");
const logger = require("morgan")
const job = require("../services/newsJob")
const bodyParser = require('body-parser');
const articles = require("../app/routes/articles")


const start = (eventEmitter) => {
  app.use(cors());
  app.use(logger('combined'));
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  app.use("/api", articles);
  
  app.get('/api/refresh', sse.init);

  eventEmitter.on('createdArticles', docs => {
    sse.send(docs);
  });

  app.listen(3001, () => {
    console.log("app listening")
    job.start()
  })
}

module.exports = start;