const express = require('express')
let router = express.Router();
const {UpdateArticle, findAndSortByDate} = require("../../context/newsRepository")

router
  .route("/articles")
  .get(async (req, res) =>{
    let dateFrom = req.query.dateFrom;
    let news = await findAndSortByDate(dateFrom);
    res.json(news)
  })
  .put(async (req, res) => {
    const articleId = req.body.articleId
    let dbResponse = await UpdateArticle(articleId);
    res.json({data: dbResponse})
  })

module.exports = router;