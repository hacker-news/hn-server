const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NewsSchema = new Schema({
  objectID: String,
  created_at: {type:Date, index: true},
  title: String,
  url: String,
  author: String,
  points: Number,
  story_text: String,
  comment_text: String,
  num_comments: Number,
  story_id: {type:Number, index: { unique: true, dropDups: true }},
  story_title: String,
  story_url: String,
  parent_id: Number,
  created_at_i: {type:Number, index: true},
  visible: {type:Boolean, default: true}
})

module.exports = mongoose.model('News', NewsSchema);