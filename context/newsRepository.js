const moment = require('moment');
const {eventEmitter} = require('../events/event-emitter')
let News = require('./models/newsModel');

let firstLoad = true;

  async function loadNews(news){
    return await News.insertMany(news, { ordered: false}, (err, docs) => {
      
      if(err && err.name === "BulkWriteError"){
      console.error(
        {
          error: err.name,
          totalWriteErrors: err.writeErrors.length,
          insertedDocs: err.insertedDocs.length
        })

        if(err.insertedDocs.length > 0 && firstLoad == false){
          console.log("emitting CretedArticles event...")
          eventEmitter.emit('createdArticles', err.insertedDocs);
        }
        firstLoad = false;
      }else{
        console.log({CreatedArticles: docs.length})
        if(!firstLoad){
          console.log("emitting CretedArticles event...")
          eventEmitter.emit('createdArticles', docs);
        } 
        firstLoad = false;
      }
    });
  }
  
  async function UpdateArticle(id){
    return await News.updateOne({_id: id}, { visible: false}, {
      new: true
    }, (err, doc) => {
      return doc;
    })
  }

  async function findAndSortByDate(date){
    if(date){
      console.log(date)
      return await News.find({
        "created_at": {"$gte": date},
        "visible": { "$ne": false }
      })
      .sort({created_at_i: -1})
    }
    else{
      return await News.find({
        "visible": { "$ne": false }
      })
      .sort({created_at_i: -1})
    }
  }

module.exports = {loadNews, findAndSortByDate, UpdateArticle};